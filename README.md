# Microsservico base

Este projeto serve como referencia para aplicacoes do OpenFinance Bradesco Fase4. O README apresentado descreve os aspectos mais importantes apra utilização desse projeto guia. Mas, em um novo projeto produtivo, ele deve servur para trazer ao desenvolvedor a finalidade daquele servico com exemplos, diagrama de sequencia, objetos JSON e instrucoes de startup do servico.

## Arquitetura

O modelo de arquitetura (modelo de organização de pastas e código que o programa deve seguir) tem como base o Clean Architecture. 

- domain - inclui os objetos de negócio e as regras de negócio;
- entrypoint - responsável por lidar com a solicitação do usuário;
- external - acesso e recursos técnicos como bibliotecas para acessar API de terceiros ou banco de dados;
- usecase - contém regras de negócios específicas do software.

## Dependencias

- Kafka
- Lombok

## Documentacao

Os padrões de documentação estão documentados com mais detalhes nesse artigo no [Confluence](https://confluence.bradesco.com.br:8443/pages/viewpage.action?pageId=149111420 "Confluence")

## Utilizando esse serviço para padronizar o seu trabalho

Para padronização entre os serviços, voce pode utilizar como referência os arquivos deste projeto. Você pode cloná-lo em seu repositório local ou baixar o pacote compactado. 
Após clonar ou descompactar o pacote, deve seguir os seguintes passos:

- Excluir a pasta **.git**
- Renomear o projeto para sua utilização

Em seguida, para salvar seu trabalho e se conectar com o repositório remoto, execute os comando Git:

```git
git init
git remote add origin URL_REPOSITORIO
```