package br.com.bradesco.external.rest;

import br.com.bradesco.external.gateway.DirectoryParicipantGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@RequiredArgsConstructor
public class DirectoryParicipantGatewayImpl implements DirectoryParicipantGateway {

    private final RestTemplate restTemplate;

    @Override
    public ResponseEntity<String> calldirectoryParticipant() {
        return restTemplate.exchange("https://data.directory.openbankingbrasil.org.br/participants", HttpMethod.GET, null, String.class);
    }

}
