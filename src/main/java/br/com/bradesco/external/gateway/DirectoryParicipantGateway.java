package br.com.bradesco.external.gateway;

import org.springframework.http.ResponseEntity;

public interface DirectoryParicipantGateway {

    ResponseEntity<String> calldirectoryParticipant();

}
