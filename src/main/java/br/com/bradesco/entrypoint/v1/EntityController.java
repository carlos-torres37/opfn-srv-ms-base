package br.com.bradesco.entrypoint.v1;

import br.com.bradesco.usecase.EntityUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping
@RestController
@RequiredArgsConstructor
public class EntityController {

    private final EntityUseCase entityUseCase;

    @GetMapping("teste")
    public String teste() {
        return entityUseCase.chamaDiretorioParticipante();
    }

}
