package br.com.bradesco.usecase;

import br.com.bradesco.external.gateway.DirectoryParicipantGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class EntityUseCase {

    private final DirectoryParicipantGateway directoryParicipantGatewayImpl;

    public String chamaDiretorioParticipante() {
        ResponseEntity<String> response = directoryParicipantGatewayImpl.calldirectoryParticipant();
        return response.getBody();
    }
}
