package br.com.bradesco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpfnSrvMsBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpfnSrvMsBaseApplication.class, args);
	}

}
